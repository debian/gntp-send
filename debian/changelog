gntp-send (0.3.4-7) unstable; urgency=medium

  * debian/watch
    - Fix uscan error
  * debian/copyright
    - Update maintainer entry
  * debian/control
    - Bump Standards-Version to 4.6.1.

 -- Kentaro Hayashi <kenhys@xdump.org>  Tue, 15 Nov 2022 21:30:29 +0900

gntp-send (0.3.4-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * debian/control
    - Bump standard version to 4.5.1. No other changes are required.

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 02 Dec 2020 20:58:27 +0900

gntp-send (0.3.4-5) unstable; urgency=medium

  * debian/control
    - Bump debhelper version
  * debian/gntp-send.1
    - Add missing man page

 -- Kentaro Hayashi <kenhys@xdump.org>  Fri, 15 May 2020 17:37:42 +0900

gntp-send (0.3.4-4) unstable; urgency=medium

  * debian/control
    - Bump compatibility level to 12
    - Add Rules-Requires-Root: no
    - Bump standard version to 4.5.0. No other changes are required.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Fri, 14 Feb 2020 18:14:05 +0900

gntp-send (0.3.4-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add Vcs-* field

  [ Kentaro Hayashi ]
  * debian/.gitlab-ci.yml
    - Add salsa CI configuration
  * debian/compat
    - Drop needless file (Replaced by debhelper-compat)
  * debian/control
    - Replace by debhelper-compat.
    - Bump standards version to 4.2.1. No other changes are required.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Sun, 02 Dec 2018 11:03:57 +0900

gntp-send (0.3.4-2) unstable; urgency=medium

  * debian/control
    - Bump debhelper version to 11.
    - Bump standards version to 4.1.3. No other changes are required.
  * debian/compat
    - Bump compatibility level to 11.
  * debian/copyright
    - Fix insecure protocol for the URI.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Thu, 22 Feb 2018 00:48:58 +0900

gntp-send (0.3.4-1) unstable; urgency=medium

  * Initial upload to Debian (Closes: #829331)
  * debian/changelog
    - Removed past release changelog entries which are originally
      maintained by Yasuhiro Matsumoto who is upstream author of
      gntp-send. Those entries are just maintained to provide
      packages on Launchpad PPA.
      ref. https://launchpad.net/~mattn/+archive/ubuntu/gntp-send
  * debian/watch
    - Supported to watch upstream.
  * debian/upstream/signing-key.asc
    - Supported cryptographic signature.
  * debian/upstream/metadata
    - Added metadata about gntp-send package
  * debian/rules
    - Enabled hardening compile flags.
    - Supported multiarch.
  * debian/control
    - Removed needless dependency to autotools-dev because debhelper
      9.20160114 requires it.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Sun, 17 Jul 2016 16:25:53 +0900
